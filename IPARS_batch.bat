:: Image Processing with OTB from https://www.orfeo-toolbox.org/

@echo off
:: Enable delayed expansion in order to use SET in the for loop and evaluate the modifications done to the variables after the SET with !var!
setlocal enableextensions enableDelayedExpansion

:: Static paths
SET bin=C:\Users\Mihai\Desktop\IPARS\OTB-7.0.0-Win64\bin
SET in=C:\Users\Mihai\Desktop\IPARS\workspace\data
SET out=C:\Users\Mihai\Desktop\IPARS\workspace\data\batch_out
:: Make a folder and go to it because there is junk that the program creates in the current folder. Also make the output folder if it doesn't exit because the program doesn't do it
mkdir %out%
mkdir tmp & cd tmp

:: Loop through the options fpr spatialr and ranger
for /L %%s in (4, 4, 24) do (
	for /L %%r in (4,4,24) do (
		:: Call the first bat in the process. Very expensive process.
		CALL %bin%\otbcli_MeanShiftSmoothing.bat -in %in%\image1.tif -fout %out%\filtered_image_s%%s_r%%r.tif -foutpos %out%\output_position_s%%s_r%%r.tif -spatialr %%s -ranger %%r -thres 0.1 -maxiter 20
		
		:: Using half the values seems to give better results. This is why I need delayed expansion to be able to load those changes in the body of the for at the beginning of the line. Use Set /a for arithmetic operations.
		set /a ss=%%s/2
		set /a rr=%%r/2
		
		:: VERY IMPORTANT to use CALL here, or the command will screw up. This is where delayed expression expands the value at the call line if !var! is used instead of %var%
		CALL %bin%\otbcli_LSMSSegmentation.bat -in %out%\filtered_image_s%%s_r%%r.tif -inpos %out%\output_position_s%%s_r%%r.tif -out %out%\segmentation_s%%s_r%%r_ss!ss!_rr!rr!.tif -spatialr !ss! -ranger !rr! -tilesizex 250 -tilesizey 250
		
		CALL %bin%\\otbcli_ColorMapping.bat -in %out%\segmentation_s%%s_r%%r_ss!ss!_rr!rr!.tif -out %out%\segmentation_c_s%%s_r%%r_ss!ss!_rr!rr!.tif -method optimal

		for /L %%m in (1,2,6) do (
			CALL %bin%\otbcli_LSMSSmallRegionsMerging.bat -in %out%\filtered_image_s%%s_r%%r.tif -inseg %out%\segmentation_s%%s_r%%r_ss!ss!_rr!rr!.tif -out %out%\segmentation_merged_s%%s_r%%r_ss!ss!_rr!rr!_m%%m.tif -minsize %%m -tilesizex 250 -tilesizey 250	
			
			CALL %bin%\otbcli_ColorMapping.bat -in %out%\segmentation_merged_s%%s_r%%r_ss!ss!_rr!rr!_m%%m.tif -out %out%\segmentation_merged_c_s%%s_r%%r_ss!ss!_rr!rr!_m%%m.tif -method optimal
		)
		for /L %%m in (10,5,21) do (
			CALL %bin%\otbcli_LSMSSmallRegionsMerging.bat -in %out%\filtered_image_s%%s_r%%r.tif -inseg %out%\segmentation_s%%s_r%%r_ss!ss!_rr!rr!.tif -out %out%\segmentation_merged_s%%s_r%%r_ss!ss!_rr!rr!_m%%m.tif -minsize %%m -tilesizex 250 -tilesizey 250	
			
			CALL %bin%\otbcli_ColorMapping.bat -in %out%\segmentation_merged_s%%s_r%%r_ss!ss!_rr!rr!_m%%m.tif -out %out%\segmentation_merged_c_s%%s_r%%r_ss!ss!_rr!rr!_m%%m.tif -method optimal
		)
	)
)
endlocal